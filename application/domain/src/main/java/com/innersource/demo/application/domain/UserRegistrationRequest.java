package com.innersource.demo.application.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserRegistrationRequest {

	@NotNull
	@Email
	@Size(max=100)
	private String email;
	
	@NotNull
	@Size(max=10,min=6)
    private String password;
	
	@Size(min=2,max=20)
	private String firstName;
	
	@Size(min=2,max=30)
	private String lastName;

	
	
}
