package com.innersource.demo.application.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserRegistrationDto {

	private String email;
	
	private String firstName;

	private String lastName;
}
