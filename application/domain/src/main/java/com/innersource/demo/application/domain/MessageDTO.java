package com.innersource.demo.application.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown=true)
public class MessageDTO {
	
	@NotNull
	private String sender;
	
	@NotNull
	private String receiver;
	 
	@NotNull
	@Size(min=3,max=100)
	private String message;
}
