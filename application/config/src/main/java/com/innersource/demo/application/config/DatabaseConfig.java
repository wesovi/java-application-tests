package com.innersource.demo.application.config;


import com.innersource.demo.application.core.model.UserAccount;
import com.innersource.demo.application.core.model.UserMessage;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by icorrales on 10/04/15.
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.innersource.factorya.application.repository","com.innersource.factoryb.application.repository","com.innersource.factoryc.application.repository"})
@EntityScan(basePackageClasses={UserAccount.class,UserMessage.class})
public class DatabaseConfig {
}
