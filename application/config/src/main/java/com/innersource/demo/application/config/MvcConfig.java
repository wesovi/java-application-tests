package com.innersource.demo.application.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
    	registry.addViewController("/").setViewName("home");
    	registry.addViewController("/home").setViewName("home");
        registry.addViewController("/web/register").setViewName("register");
        registry.addViewController("/web/login").setViewName("login");
    }

}
