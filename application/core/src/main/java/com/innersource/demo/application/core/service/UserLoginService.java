package com.innersource.demo.application.core.service;


import com.innersource.demo.application.domain.UserLoginResponse;

public interface UserLoginService {

	UserLoginResponse login(String email, String password);
	
	 
	
}
