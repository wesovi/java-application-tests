package com.innersource.demo.application.core.service;


import com.innersource.demo.application.core.model.UserAccount;
import com.innersource.demo.application.core.repository.UserAccountRepository;
import com.innersource.demo.application.domain.UserLoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserLoginServiceImpl implements UserLoginService {

	private final UserAccountRepository userAccountRepository;

	@Autowired
	public UserLoginServiceImpl(
			UserAccountRepository userAccountRepository) {
		this.userAccountRepository = userAccountRepository;
	}

	@Override
	public UserLoginResponse login(String email,String password) {
		UserAccount userAccount = userAccountRepository.findByEmailAndPassword(email, password);
		return toUserLoginResponse(userAccount);
	}

	private UserLoginResponse toUserLoginResponse(UserAccount userAccount) {
		UserLoginResponse dto = new UserLoginResponse();
		dto.setEmail(userAccount.getEmail());
		dto.setFirstName(userAccount.getFirstName());
		dto.setLastName(userAccount.getLastName());
		return dto;
	}

}
