package com.innersource.demo.application.core.service;


import com.innersource.demo.application.domain.UserRegistrationDto;
import com.innersource.demo.application.domain.UserRegistrationRequest;

public interface UserRegistrationService {

	UserRegistrationDto findByEmail(String email);

	UserRegistrationDto createUserAccount(UserRegistrationRequest request);
	
	 
	
}
