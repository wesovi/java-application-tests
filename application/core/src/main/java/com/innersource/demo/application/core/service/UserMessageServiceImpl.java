package com.innersource.demo.application.core.service;


import com.innersource.demo.application.core.model.UserMessage;
import com.innersource.demo.application.core.repository.UserMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Calendar;

@Service
public class UserMessageServiceImpl implements UserMessageService{

	@Autowired
	private UserMessageRepository userMessageRepository;
	
	@Override
	public void sendMessage(BigInteger senderId, BigInteger receiverId,
			String message) {
		UserMessage userMessage = new UserMessage();
		userMessage.setSenderId(senderId);
		userMessage.setReceiverId(receiverId);
		userMessage.setMessage(message);
		userMessage.setCreationDate(Calendar.getInstance().getTime());
		userMessageRepository.save(userMessage);
	}
	
	

}
