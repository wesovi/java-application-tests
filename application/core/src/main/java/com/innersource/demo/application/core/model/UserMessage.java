package com.innersource.demo.application.core.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;


@Data
@NoArgsConstructor
@Entity
@Table(name="USER_MESSAGE")
public class UserMessage {

	@Id
	@Column(name="ID")
    @GeneratedValue
    private BigInteger id;
	
	@Column(name="SENDER_ID")
    private BigInteger senderId;
	
	@Column(name="RECEIVER_ID")
    private BigInteger receiverId;
	
	@Column(name="MESSAGE",length=100,nullable=false)
	private String message;
	
	@Column(name="CREATION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	
}
