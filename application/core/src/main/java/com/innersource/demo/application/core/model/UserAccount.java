package com.innersource.demo.application.core.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;


@Data
@NoArgsConstructor
@Entity
@Table(name="USER_ACCOUNT")
public class UserAccount {

	@Id
	@Column(name="ID")
    @GeneratedValue
    private BigInteger id;
	
	@Column(name="EMAIL",length=150,nullable=false,unique=true)
	private String email;
	
	@Column(name="PASSWORD",nullable=false)
	private String password;
	
	@Column(name="FIRSTNAME",length=150,nullable=false)
	private String firstName;
	
	@Column(name="LASTNAME",length=150,nullable=false)
	private String lastName;
	
	@Column(name="CREATIONDATE",nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
}
