package com.innersource.demo.application.core.service;



import com.innersource.demo.application.core.model.UserAccount;
import com.innersource.demo.application.core.repository.UserAccountRepository;
import com.innersource.demo.application.core.util.BadRequestException;
import com.innersource.demo.application.core.util.NotValidEmailException;
import com.innersource.demo.application.core.util.ResourceNotFoundException;
import com.innersource.demo.application.core.util.UserAlreadyExistsException;
import com.innersource.demo.application.domain.UserRegistrationDto;
import com.innersource.demo.application.domain.UserRegistrationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Calendar;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

    @Autowired
	private UserAccountRepository userAccountRepository;




	@Override
	public UserRegistrationDto findByEmail(String email) {
		validateEmail(email);
		UserAccount userAccount = userAccountRepository.findByEmail(email);
		validateUserAccount(userAccount);
		return toUserRegistrationDto(userAccount);
	}


    private void validateEmail(String email) {
		if (StringUtils.isEmpty(email)) {
			throw new NotValidEmailException();
		}
	}

	private void validateUserAccount(UserAccount userAccount) {
		if (userAccount == null) {
			throw new ResourceNotFoundException();
		}
	}
	
	private void validateUserAccountCreationg(UserRegistrationRequest userRegistrationRequest) {
		if (userRegistrationRequest == null) {
			throw new BadRequestException();
		}
	}

	@Override
	public UserRegistrationDto createUserAccount(UserRegistrationRequest request) {
		validateUserAccountCreationg(request);
		UserAccount userAccount = toUserAccount(request);
		userAccount.setCreationDate(Calendar.getInstance().getTime());
		try {
			userAccountRepository.save(userAccount);
			return toUserRegistrationDto(userAccount);
		} catch (DataIntegrityViolationException ex) {
			
			throw new UserAlreadyExistsException();
		}
	}

	private UserAccount toUserAccount(UserRegistrationRequest request) {
		UserAccount userAccount = new UserAccount();
		userAccount.setEmail(request.getEmail());
		userAccount.setFirstName(request.getFirstName());
		userAccount.setLastName(request.getLastName());
		userAccount.setPassword(request.getPassword());
		return userAccount;
	}

	private UserRegistrationDto toUserRegistrationDto(UserAccount userAccount) {
		UserRegistrationDto dto = new UserRegistrationDto();
		dto.setEmail(userAccount.getEmail());
		dto.setFirstName(userAccount.getFirstName());
		dto.setLastName(userAccount.getLastName());
		return dto;
	}

}
