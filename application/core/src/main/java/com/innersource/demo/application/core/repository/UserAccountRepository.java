package com.innersource.demo.application.core.repository;

import com.innersource.demo.application.core.model.UserAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;


@Repository
public interface UserAccountRepository extends CrudRepository<UserAccount, BigInteger> {

	UserAccount findByEmail(String email);

    UserAccount findByEmailAndPassword(String email, String password);
}
