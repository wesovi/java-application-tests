package com.innersource.demo.application.core.repository;


import com.innersource.demo.application.core.model.UserMessage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;


@Repository
public interface UserMessageRepository extends CrudRepository<UserMessage, BigInteger> {

}
