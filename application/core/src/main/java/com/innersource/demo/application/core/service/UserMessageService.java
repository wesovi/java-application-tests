package com.innersource.demo.application.core.service;

import java.math.BigInteger;

public interface UserMessageService {
	
	public void sendMessage(BigInteger senderId, BigInteger receiverId, String message);
}
