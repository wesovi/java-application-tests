package com.innersource.demo.application.core.service.config;

import com.innersource.demo.application.core.model.UserAccount;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by icorrales on 15/04/15.
 */
@ComponentScan("com.innersource")
@Configuration
@EntityScan(basePackageClasses=UserAccount.class)
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = {"com.innersource.demo.application.core.repository"})
public class CoreApplicationConfig {
}
