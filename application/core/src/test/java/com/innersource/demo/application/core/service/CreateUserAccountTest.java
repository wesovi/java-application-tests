package com.innersource.demo.application.core.service;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.innersource.demo.application.core.service.config.CoreApplicationConfig;
import com.innersource.demo.application.core.util.BadRequestException;
import com.innersource.demo.application.core.util.UserAlreadyExistsException;
import com.innersource.demo.application.domain.UserRegistrationRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@SpringApplicationConfiguration(classes = CoreApplicationConfig.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@DatabaseSetup(CreateUserAccountTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { CreateUserAccountTest.DATASET })
public class CreateUserAccountTest {

	private static final String FIRST_NAME = "John";

	private static final String LAST_NAME = "Doe";

	protected static final String DATASET = "classpath:datasets/user_accounts.xml";

	private static final String EMAIL = "user001@email.com";

	@Autowired
	private UserRegistrationService userRegistrationService;
	
	
	@Test(expected=UserAlreadyExistsException.class)
	public void testUserCreationWhenUserAlreadyExists() {
		UserRegistrationRequest userRegistrationRequest = createUserRegistrationRequest(EMAIL,FIRST_NAME,LAST_NAME,"secret");
		userRegistrationService.createUserAccount(userRegistrationRequest);
	}
	
	@Test(expected=BadRequestException.class)
	public void testUserCreationWhenUserIsNull(){
		userRegistrationService.createUserAccount(null);
	}
	
	
	private UserRegistrationRequest createUserRegistrationRequest(String email,String firstName,String lastName,String password){
		UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
		userRegistrationRequest.setEmail(email);
		userRegistrationRequest.setFirstName(firstName);
		userRegistrationRequest.setLastName(lastName);
		userRegistrationRequest.setPassword(password);
		return userRegistrationRequest;
	}
}
