package com.innersource.demo.application.core.service;


import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

import com.innersource.demo.application.core.service.config.CoreApplicationConfig;
import com.innersource.demo.application.core.util.NotValidEmailException;
import com.innersource.demo.application.core.util.ResourceNotFoundException;
import com.innersource.demo.application.domain.UserRegistrationDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringApplicationConfiguration(classes = CoreApplicationConfig.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@DatabaseSetup(FindUserByEmailTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { FindUserByEmailTest.DATASET })
public class FindUserByEmailTest {
	
	private static final String FIRST_NAME = "John";
	
	private static final String LAST_NAME = "Doe";

	protected static final String DATASET = "classpath:datasets/user_accounts.xml";
	
	private static final  String EMAIL ="user001@email.com";

	@Autowired
	private UserRegistrationService userRegistrationService;

	@Test
	public void testUserFound() {
		UserRegistrationDto userAccount = userRegistrationService.findByEmail(EMAIL);
		assertNotNull (userAccount);
		assertEquals(EMAIL,userAccount.getEmail());
		assertEquals(FIRST_NAME,userAccount.getFirstName());
		assertEquals(LAST_NAME,userAccount.getLastName());
	}
	
	@Test(expected=ResourceNotFoundException.class)
	public void testUserNotFound(){
		userRegistrationService.findByEmail(EMAIL+"2");
	}
	
	@Test(expected=NotValidEmailException.class)
	public void testEmailIsEmpty(){
		userRegistrationService.findByEmail(null);
	}
	

}
