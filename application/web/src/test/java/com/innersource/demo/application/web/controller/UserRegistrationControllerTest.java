package com.innersource.demo.application.web.controller;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

import com.innersource.demo.application.core.service.UserRegistrationService;
import com.innersource.demo.application.domain.UserRegistrationDto;
import com.innersource.demo.application.domain.UserRegistrationRequest;
import com.innersource.demo.application.web.controller.config.CoreApplicationConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringApplicationConfiguration(classes = CoreApplicationConfig.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRegistrationControllerTest {

	private MockMvc mockMvc;
	
	private HttpMessageConverter mappingJackson2HttpMessageConverter;
	
	@Mock
	private UserRegistrationService userRegistrationService;

	@Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
		for(HttpMessageConverter<?> converter : converters){
			if(converter instanceof MappingJackson2HttpMessageConverter){
				this.mappingJackson2HttpMessageConverter = converter;
			}
		}

       
    }
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Mockito.reset(userRegistrationService);
		mockMvc = MockMvcBuilders.standaloneSetup(
                new UserRegistrationController(userRegistrationService))
				.build();
	}

	@Test
	public void testUserCreationWithGet() throws Exception {
		mockMvc.perform(
				get(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)).andExpect(
				status().isMethodNotAllowed());
	}
	
	@Test
	public void testUserCreationWithPut() throws Exception {
		mockMvc.perform(
				put(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)).andExpect(
				status().isMethodNotAllowed());
	}
	
	@Test
	public void testUserCreationWithDelete() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest("test@mail.com","John","Doe","secret");
		mockMvc.perform(
				delete(UserRegistrationController.W_USER_REGISTRATION_PATH)
				.contentType(MediaType.APPLICATION_JSON).accept(
						MediaType.APPLICATION_JSON)
						.content(this.json(request))).andExpect(
				status().isMethodNotAllowed());
	}
	
	@Test
	public void testUserWithEmptyBody() throws Exception {
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
				.contentType(MediaType.APPLICATION_JSON).accept(
						MediaType.APPLICATION_JSON)
						).andExpect(
				status().isBadRequest());
	}
	
	@Test
	public void testUserWithEmptyEmail() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest(null,"John","Doe","secret");
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)
								.content(this.json(request))).andExpect(
				status().isBadRequest());
	}
	
	@Test
	public void testUserWithNoValidEmail() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest("lalalal","John","Doe","secret");
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)
								.content(this.json(request))).andExpect(
				status().isBadRequest());
	}
	
	@Test
	public void testUserWithNoPassword() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest("test@mail.com","John","Doe",null);
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)
								.content(this.json(request))).andExpect(
				status().isBadRequest());
	}
	
	@Test
	public void testUserWithNoValidPassword() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest("test@mail.com","John","Doe","sec");
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)
								.content(this.json(request))).andExpect(
				status().isBadRequest());
	}
	
	@Test
	public void testUserWithNoValidFirstname() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest("test@mail.com",".","Doe","secret");
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)
								.content(this.json(request))).andExpect(
				status().isBadRequest());
	}
	
	@Test
	public void testUserWithNoValidLastname() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest("test@mail.com","John","D","secret");
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)
								.content(this.json(request))).andExpect(
				status().isBadRequest());
	}
	
	@Test
	public void testUserAlreadyExists() throws Exception {
		UserRegistrationRequest request = createUserRegistrationRequest("testingit@email.com","John","Doe","secret");
		Mockito.when(userRegistrationService.findByEmail("testingit@email.com")).thenReturn(new UserRegistrationDto());
		mockMvc.perform(
				post(UserRegistrationController.W_USER_REGISTRATION_PATH)
						.contentType(MediaType.APPLICATION_JSON).accept(
								MediaType.APPLICATION_JSON)
								.content(this.json(request))).andExpect(
				status().isConflict());
	}
	
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
	
	
	
	private UserRegistrationRequest createUserRegistrationRequest(String email,String firstName,String lastName,String password){
		UserRegistrationRequest request = new UserRegistrationRequest();
		request.setEmail(email);
		request.setFirstName(firstName);
		request.setLastName(lastName);
		request.setPassword(password);
		
		return request;
	}
}
