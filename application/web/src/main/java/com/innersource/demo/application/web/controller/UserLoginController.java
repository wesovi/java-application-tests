package com.innersource.demo.application.web.controller;


import com.innersource.demo.application.core.service.UserLoginService;
import com.innersource.demo.application.core.util.EmailNotValidException;

import com.innersource.demo.application.domain.UserLoginRequest;
import com.innersource.demo.application.domain.UserLoginResponse;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Log4j
@Controller
@RequestMapping(UserLoginController.W_USER_REGISTRATION_PATH)
public class UserLoginController {
	
	public static final String W_USER_REGISTRATION_PATH = "/users";
	
	private final UserLoginService userLoginService;
	
	@Autowired
	public UserLoginController(UserLoginService userLoginService){
		this.userLoginService = userLoginService;
	}
	
	
	@RequestMapping(value="/auth",method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseStatus(value = HttpStatus.CREATED)
	public @ResponseBody
    UserLoginResponse doLogin(@RequestBody @Valid UserLoginRequest request){
		log.debug("start user registration");
		return userLoginService.login(request.getEmail(),request.getPassword());
	}
	
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(value=EmailNotValidException.class)
    public String exception(EmailNotValidException e) {
        return "UserAlreadyExists";
    }
	

	
}



