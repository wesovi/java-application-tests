package com.innersource.demo.application.web.controller;

import com.innersource.demo.application.core.service.UserMessageService;

import com.innersource.demo.application.domain.MessageDTO;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.math.BigInteger;


@Log4j
@RestController
@RequestMapping(SendMessageController.W_SEND_MESSAGE_PATH)
public class SendMessageController {

	public static final String W_SEND_MESSAGE_PATH = "/messages";

	private final UserMessageService userMessageService;

	@PostConstruct
	  public void init() {
	    log.warn("Controller loaded.");
	  }
	
	@Autowired
	public SendMessageController(UserMessageService userMessageService) {
		this.userMessageService = userMessageService;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void sendMessage(@RequestBody @Valid MessageDTO request) {
		userMessageService.sendMessage(BigInteger.valueOf(Long.valueOf(request.getSender())), BigInteger.valueOf(Long.valueOf(request.getReceiver())),request.getMessage());
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value=RuntimeException.class)
    public String exception(RuntimeException  e) {
        return "Unknown Error";
    }
}
