package com.innersource.demo.application.web.controller;



import com.innersource.demo.application.core.service.UserRegistrationService;
import com.innersource.demo.application.core.util.ResourceNotFoundException;
import com.innersource.demo.application.core.util.UserAlreadyExistsException;
import com.innersource.demo.application.domain.UserRegistrationDto;
import com.innersource.demo.application.domain.UserRegistrationRequest;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Log4j
@RestController
@RequestMapping(UserRegistrationController.W_USER_REGISTRATION_PATH)
public class UserRegistrationController {
	
	public static final String W_USER_REGISTRATION_PATH = "/users";
	
	private final UserRegistrationService userRegistrationService;
	
	@Autowired
	public UserRegistrationController(UserRegistrationService userRegistrationService){
		this.userRegistrationService = userRegistrationService;
	}
	
	
	@RequestMapping(value="",method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseStatus(value = HttpStatus.CREATED)
	public @ResponseBody
    UserRegistrationDto doRegistration(@RequestBody @Valid UserRegistrationRequest request){
		log.debug("start user registration");
		validate(request);
		return userRegistrationService.createUserAccount(request);
		
	}
	
	@ResponseStatus(value = HttpStatus.CONFLICT)
	@ExceptionHandler(value=UserAlreadyExistsException.class)
    public String exception(UserAlreadyExistsException e) {
        return "UserAlreadyExists";
    }
	

	private void validate(UserRegistrationRequest request) {
		validateIfUserIsAlreadyRegistered(request.getEmail());
	}

	private void validateIfUserIsAlreadyRegistered(String email) {
		try{
			userRegistrationService.findByEmail(email);
			throw new UserAlreadyExistsException();
		}catch(ResourceNotFoundException ex){
			
		}
	}
}



